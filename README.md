# CounselHQ Add-in
## Instant access to relevant content from your organization’s documents as you type.
CounselHQ is designed for professionals and organizations that frequently work on similar types of documents, whether that’s contracts, sales proposals, marketing materials, research articles, or something else. We help you rely on your past work product so you don't have to reinvent the wheel with every new document. Work faster and ensure consistency across your documents and organization with CounselHQ.

The `counselhq_addin.xml` file is the source code for CounselHQ's official add-in for Microsoft Word.

Please visit [our website](https://www.counselhq.com) for more information.

## License
This software is released under the Apache License, Version 2.0 (see LICENSE.txt).